import React from 'react';

// Creates a context object
	// A context object, as the name states, is a data type of an object that can be used to store information that can be shared with other components within the app.
	// The context object object is a different approach to passing information between components and allows us for easier access by avoiding the sue of prop drilling.

const UserContext = React.createContext();

// The "Provider" component allows other components to consume the context object and supply the necessary information needed to the context object.

export const UserProvider = UserContext.Provider;

export default UserContext;