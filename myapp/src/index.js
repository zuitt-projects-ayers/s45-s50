import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// const name = "Jason Ayers";

// const user = {
//   firstName : "Chris",
//   lastName : "Pratt"
// };

// const formatName = (user) => {
//   return `${user.firstName} ${user.lastName}`
// }

// // JSX
// const element = <h1>Hello! My name is {formatName(user)}.</h1>

// ReactDOM.render(
//     element,
//     document.getElementById("root")
// );