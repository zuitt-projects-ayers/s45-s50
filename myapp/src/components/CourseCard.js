import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){

	//console.log(courseProp);
		// expected result is coursesData[0]
	//console.log(typeof courseProp);

	// const [count, setCount] = useState(0);
	// // syntax: const [getter, setter] = useState(initialValueOfGetter);
	// // (Apparently, the setter is already defined to change the getter value.)

	// const [seats, setSeats] = useState(30);

	// function enroll(){
	// 	if (seats > 0) {
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 		console.log('Enrollees: ' + count);
	// 		console.log('Seats: ' + seats);
	// 	}
	// };

	// // syntax: useEffect(() => {}, [optionalParameter])
	// // (This runs every time there is an update with DOM (and when the react app starts(?)).)
	// useEffect(() => {
	// 	if (seats === 0) {
	// 		alert("No more seats available.");
	// 	}
	// }, [seats]);

	const {name, description, price, _id} = courseProp

	return(
		<Card className="courseCard p-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
{/*				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count}</Card.Text>*/}
				<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details</Button>
			</Card.Body>
		</Card>
	)
};